"""
Functions to help implementing REST API for our backends.
"""

import json

def response(resp, success=True):
    """Create and return a dictionary with what was defined as standard for 
    a REST response.

    Keyword arguments:
    resp -- Value for the key 'response', which will be the response showed 
    in the front-end. It can be a string, a list, or a dictionary, as this 
    is the value in JSON format.
    success -- True or False: successfulness of the operation.

    Return a dictionary of this form:
        {'success': <True|False>, 'response': <response>}

    Main purpose for this function is to be used as the response to be sent 
    to a web front-end, but it can be used for any standardized REST response.
    """
    try:
        print(json.dumps({'success': success, 'response': resp}))
    except Exception as e:
        error = "Error ocurred when dumping response into a JSON-REST response"
        print(json.dumps(
            {
                'success': False, 
                'response': '{0}: {1}'.format(error, str(e))
            })
        )
