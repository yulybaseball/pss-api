"""
Contains useful functions for dictionaries, lists, tuples, time-related stuff.
"""

import re

def update_dictionary(receiver_dict, provider_dict):
    """Update 'receiver_dict' with the key/values in 'provider_dict'.
    Do not replace the entire key in 'receiver_dict'; only replace if 
    key/value are exactly the same, in which case only the value will be 
    replaced.
    """
    import collections
    for k, dummy in provider_dict.items():
        if (k in receiver_dict and isinstance(receiver_dict[k], dict)
                and isinstance(provider_dict[k], collections.Mapping)):
            update_dictionary(receiver_dict[k], provider_dict[k])
        else:
            receiver_dict[k] = provider_dict[k]

def is_json(obj, file=True):
    """Return True if obj is a valid json file or content. False otherwise."""
    import json
    try:
        if file:
            with open(obj) as json_file:
                json.load(json_file)
        else:
            json.loads(obj)
    except:
        return False
    return True

def bool_from_txt(txt, casesensitive=False):
    """Return the bool representation of txt, according to predefined False 
    and True values.

    It only admits booleans and strings. Anything else will result in raising 
    a ValueError exception.
    """
    if isinstance(txt, bool):
        return txt
    try:
        false_values = ["0", "false", "False"]
        true_values = ["1", "true", "True"]
        resulttxt = txt if casesensitive else txt.lower()
        if resulttxt in false_values:
            return False
        elif resulttxt in true_values:
            return True
        raise ValueError("Value does not comply with False nor True.")
    except Exception as e:
        raise ValueError(str(e))

def get_elapse_mins(start_time, finish_time):
    """Calc and return the minutes between 'start_time' and 'finish_time'."""
    try:
        return "{0:.1f}".format((finish_time - start_time).seconds / 60.0)
    except:
        return ""

def get_values2format(frm_str):
    """Return a list of the values between curly brackets in 'frm_str'."""
    regex = r"\{(.*?)\}"
    return re.findall(regex, frm_str)

def tabled_html_data(data, headers, enablesort=False):
    """Format and return a string representing an HTML table.

    Keyword arguments:
    data -- List containing other lists, which represent the rows on the 
    table. Please note the order of the values should match the same order of 
    the column name.
    headers -- List of the header names.
    enablesort -- Defines if columns can be sorted by clicking on the header 
    row.

    Table is generated without any style (no CSS applied: no borders, default 
    aligments, default text...).
    """
    table_template = ("<table id=\"utilPyTable\" class=\"sortable\">{0}" + 
                      "<tbody>{1}</tbody></table>")
    tr_template = "<tr>{0}</tr>\n"
    td_template = "<td>{0}</td>"
    if enablesort:
        th_template = "<th onclick=\"sortTable({0})\">{1}</th>"
        hr_template = ("<thead><tr>{0}</tr></thead>").\
                  format("".join(th_template.format(i, a) \
                      for i, a in enumerate(headers)))
    else:
        th_template = "<th>{0}</th>"
        hr_template = ("<thead><tr>{0}</tr></thead>").\
                  format("".join(th_template.format(a) for a in headers))
    subitems = [tr_template.format(''.\
               join([td_template.format(a) for a in item])) for item in data]
    return table_template.format(hr_template, "".join(subitems))

def tabled_data(data, headers, sep='\uFFFA'):
    """Format and return a pretty printed table in plain text.

    Keyword arguments:
    data -- List containing other lists, which represent the rows on the 
    table. Please note the order of the values should match the same order of 
    the column name.
    headers -- List of the header names.
    sep -- Character to separate text inside the cell. Dummy character by 
    default.
    """
    table_str = ""
    sep = str(sep) if sep else '\n'
    all_data = []
    all_data.append(headers)
    for row in data:
        all_data.append(row)
    col_size = [
        max(map(len, (sep.join(col)).split(sep))) for col in zip(*all_data)
    ]
    frmt_str = ' | '.join(["{{{0}:<{1}}}".format(i, charlen) \
                            for i, charlen in enumerate(col_size)])
    line = frmt_str.replace(' | ', '-+-').format(*['-' * i for i in col_size])
    item = all_data.pop(0)
    line_done = False
    while all_data:
        if all(not i for i in item):
            item = all_data.pop(0)
            if line and (sep != '\uFFFA' or not line_done):
                table_str += (line + '\n')
                line_done = True
        row = [i.split(sep,1) for i in item]
        table_str += ((frmt_str.format(*[i[0] for i in row])) + '\n')
        item = [i[1] if len(i) > 1 else '' for i in row]
    return table_str

def get_elapsed_time(startdatetime, finishdatetime, **kwargs):
    """Calculate the time elapsed between 2 datetimes.

    Keyword arguments:
    startdatetime -- Datetime older in time (object from datetime.now()).
    finishdatetime -- Datetime closer in time (object from datetime.now()).
    formattype -- String representing the format of the result returned. It 
    can take 3 values: 
    - by default, this will return a string of this form:
        <d> days, <h> hours, <m> minutes, <s> seconds
        omitting whichever has value 0.
    - 'flist': will return a list of this form:
        [<days>, <hours>, <minutes>, <seconds>]
    - 'fdict': will return a dictionary of this form:
        {'days': <d>, 'hours': <h>, 'minutes': <m>, 'seconds': <s>}
    includevals -- String containing the values to be returned as string.
    Please note this argument will be used only when the value of 
    'formattype' is empty or 'fstr'. By default, all values are included, 
    which is the same as passing in this string (in any order): 'dhms'. 
    To exclude one value, just remove the corresponding letter.
    """
    from datetime import timedelta
    time_delta = (finishdatetime - startdatetime)
    td = timedelta(seconds=time_delta.seconds, days=time_delta.days)
    secs = td.seconds % 60
    mins = int((td.seconds // 60) % 60)
    hours = int(td.seconds // 3600)
    days = td.days
    fr = []
    td_dict = [
        {'day': days}, 
        {'hour': hours}, 
        {'minute': mins}, 
        {'second': secs}
    ]
    formattype = kwargs['formattype'] if 'formattype' in kwargs else ""
    if formattype == 'fdict':
        return {'days': days, 'hours': hours, 'minutes': mins, 'seconds': secs}
    if formattype == 'flist':
        return [days, hours, mins, secs]
    includevals = kwargs['includevals'] if 'includevals' in kwargs else "dmhs"
    for tm in td_dict:
        dkey = next(iter(tm))
        td_count = tm[dkey]
        if td_count >= 1 and (dkey[0] in includevals):
            str_d = dkey if td_count == 1 else "{0}s".format(dkey)
            fr.append("{0} {1}".format(td_count, str_d))
    return ", ".join(fr)

def size_frmter(bytes_count, suffix='B'):
    """Format bytes into the closer measurement."""
    for unit in ['', 'K', 'M', 'G', 'T', 'P', 'E', 'Z']:
        if abs(bytes_count) < 1024.0:
            return "%3.1f %s%s" % (bytes_count, unit, suffix)
        bytes_count /= 1024.0
    return "%.1f%s%s" % (bytes_count, 'Y', suffix)

def is_binary(_file):
    """Determine whether _file is binary or not.
    Please note this function might have some false positives and false 
    negatives, so use it as a helper to another checker alternative.
    """
    textchars = bytearray(
        set([7,8,9,10,12,13,27]) | set(range(0x20, 0x100)) - set([0x7f]))
    is_binary_string = lambda bytes: bool(bytes.translate(None, textchars))
    isb = False
    with open(_file, 'rb') as _f:
        isb = is_binary_string(_f.read(1024))
    return isb

def file_hash(_file, algorithm='md5'):
    """Get and return the hash of _file using the algorithm passed in."""
    import hashlib
    fn = getattr(hashlib, algorithm, None)
    if not fn:
        return "Algorithm '{0}' is not available.".format(algorithm)
    BLOCKSIZE = 65536
    try:
        hasher = fn()
        with open(_file, 'rb') as afile:
            buf = afile.read(BLOCKSIZE)
            while len(buf) > 0:
                hasher.update(buf)
                buf = afile.read(BLOCKSIZE)
        return hasher.hexdigest()
    except Exception as e:
        return str(e)

def sort_alphanumeric(list2be_sorted):
    """Sort list2be_sorted taking into account alphanumeric values."""
    convert = lambda text: float(text) if text.isdigit() else text
    alphanum = lambda key: [convert(c) for c in re.split(
                                                '([-+]?[0-9]*\.?[0-9]*)', key)]
    list2be_sorted.sort(key=alphanum)
    return list2be_sorted

def bytes2str(text):
    """Convert and return text to string if it is of type bytes."""
    if isinstance(text, bytes):
        try:
            return text.decode('utf-8')
        except Exception:
            pass
    return text
