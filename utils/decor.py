"""
Contains useful decorators.
"""

import signal

def onterminated(exit_func):
    """Set up the function 'exit_func' as the one to be executed when the 
    script is terminated.
    """
    def _signal_wrapper(func):
        def _setup_signals():
            signal.signal(signal.SIGTERM, exit_func)
            signal.signal(signal.SIGABRT, exit_func)
            signal.signal(signal.SIGINT, exit_func)
            return func()
        return _setup_signals
    return _signal_wrapper
