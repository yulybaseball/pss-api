"""
Module to help working with multithreading and multiprocessing.
"""

import threading

class ThreadSafeDict(dict) :
    """
    Represents a dictionary which is safe to access from different threads.
    """
    def __init__(self, *args, **kwargs) :
        dict.__init__(self, *args, **kwargs)
        self._lock = threading.Lock()

    def __enter__(self) :
        self._lock.acquire()
        return self

    def __exit__(self, type, value, traceback) :
        self._lock.release()

class ThreadSafeList(list) :
    """
    Represents a list which is safe to access from different threads.
    """
    def __init__(self, *args, **kwargs) :
        list.__init__(self, *args, **kwargs)
        self._lock = threading.Lock()

    def __enter__(self) :
        self._lock.acquire()
        return self

    def __exit__(self, type, value, traceback) :
        self._lock.release()
