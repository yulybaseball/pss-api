"""
Contains util functions related to servers and connections.
"""

import socket

def current_server():
    """Get and return the name of current server. Convert name to string and 
    remove all break lines and spaces. On error, return empty string.
    """
    server = ""
    try:
        server = socket.gethostname()
        server = server.decode('utf-8')
        server = server.lower().replace('\r', '').replace('\n', '').strip()
    except:
        pass
    return server

def is_port_open(server, port, timeout=5):
    """Check if 'port' is open in 'server'."""
    s = socket.socket()
    s.settimeout(timeout)
    try:
        s.connect((server, port))
        return True
    except:
        return False
    finally:
        s.close()

def val_from_bash(var, **kwargs):
    """Retrive the value of 'var' in a bash script.

    Keyword arguments:
    var -- name of the variable in the bash script.
    kwargs -- possible options:
        list -- returns a list if this is True. False by default.
        split -- final result will be split by the character specified in this 
        option, and returning value will be a list, even if the 'list' option 
        is False. By default, this option is None. If an empty string is 
        provided as separator, a ValueError will be raised.
        bashscript -- complete path and name of the bash script in which var 
        will be searched at. By default, the script is 
        /home/weblogic/scripts/lib/staticDefsProd.sh
        Please note if script doesn't exist, empty string will be returned.
        server -- if defined, 'var' will be looked for in a bash script 
        located in 'server'; localhost by default.

    Returns the value of 'var', or empty string if 'var' doesn't exist.
    """
    import subprocess
    DEFAULT_BASH_SCRIPT = "/home/weblogic/scripts/lib/staticDefsProd.sh"
    bashscript = kwargs['bashscript'] if 'bashscript' in kwargs \
                                      else DEFAULT_BASH_SCRIPT
    server = kwargs['server'] if 'server' in kwargs else None
    command = ". {0}; echo ${1};".format(bashscript, var)
    if server:
        if server != current_server():
            command = "ssh {0} \"{1}\"".format(server, command)
            import shlex
            command = shlex.split(command)
        else:
            server = None
    p = subprocess.Popen(command, shell=(not bool(server)), 
                         stdin=subprocess.PIPE, stdout=subprocess.PIPE, 
                         stderr=subprocess.PIPE)
    stdout, dummy = p.communicate()
    try:
        result = stdout.replace('\n', '')
    except Exception:
        result = stdout.replace(b'\n', b'')
    if 'split' in kwargs and kwargs['split'] != None:
        return result.split(kwargs['split'])
    if 'list' in kwargs and kwargs['list']:
        return [result]
    return result

def current_datacenter(default="mia"):
    """Fetch and return the current datacenter. If exception is raised, return 
    the default value.
    """
    cdc = default
    try:
        cdc = val_from_bash("DEFAULT_LOCATION")
    except Exception:
        pass
    return cdc
