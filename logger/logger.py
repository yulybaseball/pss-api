"""
Defines specific formatter and creates logger with that specific formatter.
"""

import logging
from os import path
import sys

EXEC_MOD_DIR = path.dirname(path.realpath(sys.argv[0]))
EXEC_DIR_NAME = path.basename(EXEC_MOD_DIR)
LOG_FILE = path.join(EXEC_MOD_DIR, 'default_logger.log')
LOGGER_NAME = "{0}.{1}".format(EXEC_DIR_NAME, __name__)

class CustomFormatter(logging.Formatter):
    """Create a separate formatter for messages of every type."""
    def __init__(self, fmt='%(asctime)s - %(levelname)s - %(message)s', 
                 **kwargs):
        self.fmts = kwargs
        logging.Formatter.__init__(self, fmt)

    def _set_custom_fmtter(self, fmtter):
        """Set the parent formatter to the one specified in fmt.

        *This method was needed because of different implementations of 
        logging.Formatter class on different versions of Python.*
        """
        try:
            # for current versions
            self._style._fmt = fmtter
        except AttributeError:
            try:
                # for old versions
                self._fmt = fmtter
            except:
                # default formatter
                pass

    def format(self, record):
        """Format record with formatter specified for record level in 
        self.fmts, or 'DEFAULT' formatter if record level is not especified.
        In case of exception, just return the message without formatting it.
        """
        try:
            if self.fmts:
                if record.levelname in self.fmts:
                    self._set_custom_fmtter(self.fmts[record.levelname])
                elif 'DEFAULT' in self.fmts:
                    self._set_custom_fmtter(self.fmts['DEFAULT'])
                else:
                    return record.getMessage()
            return logging.Formatter.format(self, record)
        except:
            return record.getMessage()

def load_logger(log_file_name=LOG_FILE, logger_name=LOGGER_NAME, cgi=False,
                **kwargs):
    """Create logger with name logger_name and log_file_name as the log file.
    If cgi is True, don't print any message: this script might be used on the 
    web and could be sending text to the frontend.

    Format of the messages recorded with the returned logger are specified in 
    a kwargs. Keys will contain the message level and values will have the 
    formatting string to be used to format the message.
    Allowed keys (all message level names on logging module, plus 'DEFAULT'):

        CRITICAL
        ERROR
        WARNING
        INFO
        DEBUG

    If the message level is not specified in the dict, the message will be 
    formatted according to the format coming in 'DEFAULT' key; if this key
    is not coming either, formatting for the message will be determined by
    the class *CustomFormatter* (_probably as a default formatter defined
    in the constructor_), and same behaviour is expected if kwargs is an 
    empty dict or is not coming at all.

    Example of formatter:
    logger_fmtter = {
        'DEBUG': '%(asctime)s - %(thread)s - %(message)s',
        'INFO': '%(message)s',
        'DEFAULT': '%(asctime)s - %(thread)s - %(levelname)s - %(message)s'
    }

    Return created logger.
    """
    logger = logging.getLogger(logger_name)
    logger.setLevel(logging.DEBUG)
    try:
        if not logger.handlers:
            handler_info = logging.FileHandler(log_file_name)
            handler_info.setFormatter(CustomFormatter(**kwargs))
            logger.addHandler(handler_info)
        if not cgi:
            print("Created logger with name: {0}".format(logger.name))
            print("Logs will be sent to: {0}".format(
                                            logger.handlers[0].stream.name))
    except:
        # just need to get the logger, we can ignore the exception
        pass
    return logger
