"""Creates file as email and sends it to one or multiple email addresses."""

import datetime
import os
import sys

EMAILS_FOLDER = os.path.join(os.path.dirname(os.path.abspath(__file__)), 
                            'emails')
EXEC_DIR = os.path.basename(os.path.dirname(os.path.abspath(sys.argv[0])))
SENDER = "ProductionSystemsSupport@tracfone.com"
DEST = ["ProductionSystemsSupport@tracfone.com"]
EMAIL_DATE = 666

SEND_EMAIL_COMMAND = "mailx -t < {file_path}"

def _execute_system_command(command):
    """Execute command on current server. Return 'OK', or the resulted error."""
    try:
        os.system(command)
        return "OK"
    except Exception as e:
        return str(e)

def send_email(subject, body, sender=SENDER, dest=DEST, html=False, 
               saveit=False, highpriority=False):
    """Send email.

    Keyword arguments:
    subject -- subject of the email.
    body -- body of the email. Can contain HTML tags which will be parsed if 
    html arg is True.
    sender -- string representing the person/team/group sending the email.
    dest -- list of strings being the email addresses to send the email to.
    html -- message will be sent as HTML is this is True.
    saveit -- keep a local copy of the email in EMAILS_FOLDER if this is True.
    highpriority -- email is sent with high priority if this is True.

    Return 'OK' if email was sent successfully; error message otherwise.
    """
    ext = "html" if html else "em"
    currdatetime = datetime.datetime.now().strftime("%Y%m%d_%H%M%S")
    email_file_name = '{0}_{1}.{2}'.format(EXEC_DIR, currdatetime, ext)
    full_file_path = _create_email(email_file_name, subject, body, 
                                    sender, dest, html, highpriority)
    if full_file_path:
        cmd = SEND_EMAIL_COMMAND.format(file_path=full_file_path)
        msg = _execute_system_command(cmd)
        if not saveit:  # remove email file
            _execute_system_command("rm -f {0}".format(full_file_path))
    else:
        msg = "Email could not be sent: email file doesn't exist."
    return msg

def send_existing_email_file(file_name):
    """Send file_name as email (POSIX plain email text)."""
    cmd = SEND_EMAIL_COMMAND.format(file_path=file_name)
    return _execute_system_command(cmd)

def _js_sort_table():
    """Define and return a JS function to sort columns on tables."""
    js_function = """function sortTable(n) {
        var table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
        table = document.getElementById("utilPyTable");
        switching = true;
        dir = "asc";
        while (switching)
        {
            switching = false;
            rows = table.rows;
            for (i = 1; i < (rows.length - 1); i++)
            {
                shouldSwitch = false;
                x = rows[i].getElementsByTagName("TD")[n];
                y = rows[i + 1].getElementsByTagName("TD")[n];
                if (dir == "asc")
                {
                    if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase())
                    {
                        shouldSwitch = true;
                        break;
                    }
                }
                else if (dir == "desc")
                {
                    if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase())
                    {
                        shouldSwitch = true;
                        break;
                    }
                }
            }
            if (shouldSwitch)
            {
                rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
                switching = true;
                switchcount ++; 
            }
            else
            {
                if (switchcount == 0 && dir == "asc")
                {
                    dir = "desc";
                    switching = true;
                }
            }
        }
    }"""
    return js_function

def _html(body):
    """Format and return body as HTML content."""
    html = """
            <html>
                <head>
                    <style> 
                        table, th, td {{ border: 1px solid black; 
                                         border-collapse: collapse;
                                         font-family: sans-serif;
                                         font-size:0.8em; }}
                        th, td {{ padding: 5px; }}
                        thead {{ font-weight:bold; background-color: silver }}
                        .sortable th {{ cursor: pointer; }}
                    </style>
                    <script>
                        {0}
                    </script>
                </head>
                <body style='font-family: sans-serif;
                            color: black;font-size:0.8em'>
                    {1}
                </body>
            </html>
            """.format(_js_sort_table(), body)
    return html

def _create_email(email_file_name, subject, body, sender, dest, html, hprior):
    """Create file to be used as email. Return its path."""
    dests = " ".join(dest)
    ctstr = "text/{0}; charset=UTF-8"
    priority = int(hprior)
    conttype, body =  (ctstr.format("html"), _html(body)) if html \
                    else (ctstr.format("plain"), body)
    content = (("From: {0}\n" + 
               "Date: {1}\n" + 
               "To: {2}\n" + 
               "Subject: {3}\n" + 
               "Content-Type: {4}\n" + 
               "X-Priority: {5}\n" + 
               "\n{6}\n").format(sender, EMAIL_DATE, dests, subject, 
                                conttype, priority, body))
    try:
        if not os.path.exists(EMAILS_FOLDER):
            os.makedirs(EMAILS_FOLDER)
        filepath = os.path.join(EMAILS_FOLDER, email_file_name)
        with open(filepath, 'w+') as email_file:
            email_file.write(content)
    except BaseException:
        filepath = None
    return filepath
