#!/bin/env python

"""
Sends email using the sibling module 'emailer.py'.

Usage:
./send_email.py <subject> <body> -s <sender> -d <dest> [-h <0|1|false|true>] [-k <0|1|false|true>] [-i <0|1|false|true>]
./send_email.py -f <file>

 - subject: content of the subject field in the email.
 - body: body of the email.
 - |-s|: name/email address of the sender of the email
 - |-d|: email addresses where the email will be sent to. Addresses should be 
 separated by semicolon (;), with no space between them.
 - |-h|: html format; plain text otherwise.
 - |-k|: keep email file in the system (save it).
 - |-i|: high priority. Otherwise it's normal.
 - |-f|: json or plain text email file with the configuration of all the 
 fields for sending email. See below for keys references on the json file. 
 Email plain text file is a general POSIX mailx file. Json file is 
 recommended. Files could be at any location, just pass the complete path. 
 IMPORTANT: if this option is provided, all other options and arguments will 
 be discarded.

Please note if the optional arguments are not specified, their values will be 
the default ones set on 'emailer.py' module.

If values for the email are defined in a JSON file (-f option), these keys are 
mandatory:
 - sender: text containing the name and/or address of the sender.
 - dest: list of email addresses this email will be sent to.
 - subject: the subject of the email.
 - type: plain or html (though anything not equal to 'html' will assume plain).
 - high: true or false (for high priority).
 - body: body of the email.

JSON sample:

{
    "sender": "someone@somewhere.com",
    "dest": ["somebody@someplace.com"],
    "subject": "Test",
    "type": "plain",
    "high": false,
    "body": "This is a test email."
}

"""

from os import path
import sys

import getopt

import emailer
sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))
from utils import util, conf

OPTIONS = "s:d:h:k:i:f:"
JSON_MANDATORY_KEYS = ["sender", "dest", "subject", "type", "high", "body"]

def _quit_script(err=None):
    """Quit this script. Print err if it exists."""
    if err:
        print(err)
        exit(1)
    exit(0)

def _se(subject, body, sender, dest, html=False, high=False, saveit=False):
    """Create an email file and send it.

    Keyword arguments:
    subject -- subject of the email.
    body -- body of the email.
    sender -- name and/or email address of the sender of the email.
    dest -- list containing the addresses where this email is being sent to.
    html -- if False, email will be created as plain text.
    high -- high priority email.
    saveit -- save (keep) the file created for this email.
    """
    dest = dest if isinstance(dest, list) else [dest]
    html = util.bool_from_txt(html)
    high = util.bool_from_txt(high)
    saveit = util.bool_from_txt(saveit)
    msg = emailer.send_email(subject=subject,
                             body=body,
                             sender=sender,
                             dest=dest,
                             html=html, 
                             highpriority=high,
                             saveit=saveit)
    if msg != 'OK':
        _quit_script(msg)

def _check_json(json_data):
    """Check 'json_data' has all mandatory keys."""
    if sorted(json_data.keys()) != sorted(JSON_MANDATORY_KEYS):
        _quit_script("JSON file is missing at least one mandatory key.")
    if not isinstance(json_data["dest"], list):
        _quit_script("Key 'to' in JSON file must be a list.")

def _send_email_from_json(conf_data):
    """Send email according to the conf values defined in 'conf_data'.

    Keyword arguments:
    conf_data -- dictionary containing the values to create the email file and 
    send it out.
    """
    _check_json(conf_data)
    html = (conf_data['type'] == 'html')
    _se(subject=conf_data['subject'], body=conf_data['body'],
        sender=conf_data['sender'], dest=conf_data['dest'],
        html=html, high=conf_data['high'])

def _send_email_from_posix_em(file_name):
    """Send 'file_email' as email."""
    emailer.send_existing_email_file(file_name)

def _email_from_file(file_name):
    """Use 'file_email' as the file with the conf to send an email."""
    if util.is_json(file_name):
        _send_email_from_json(conf.get_conf(file_name))
    else:
        _send_email_from_posix_em(file_name)

def _send_email(fullparams):
    """Prepare and send email according to 'fullparams'."""
    if len(fullparams) < 3:
        _quit_script("Missing parameter(s).")
    try:
        options, values = getopt.gnu_getopt(fullparams[1:], OPTIONS)
        opt_dict = {}
        for option in options:
            opt = option[0]
            optval = option[1]
            if opt == '-f':
                _email_from_file(optval)
                # discard all other options passed in to the script
                return
            else:
                opt_dict[opt] = optval
        html = opt_dict['-h'] if '-h' in opt_dict else False
        high = opt_dict['-i'] if '-i' in opt_dict else False
        saveit = opt_dict['-k'] if '-k' in opt_dict else False
        _se(subject=values[0], 
            body=values[1], 
            sender=opt_dict['-s'], 
            dest=opt_dict['-d'], 
            html=html, 
            high=high, 
            saveit=saveit)
    except getopt.error as err:
        _quit_script(str(err))

def _main():
    fullparams = sys.argv
    _send_email(fullparams)
    _quit_script()

if __name__ == "__main__":
    _main()
