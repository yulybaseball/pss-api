"""
Executes OS commands.
"""

import shlex
import socket
import subprocess

def cmdexec(command, **kwargs):
    """Execute command and return the output on this form: (stdout, stderr).
    Check kwargs['stdin'] to write its content to stdin.
    """
    p = subprocess.Popen(shlex.split(command), stdin=subprocess.PIPE,
                         stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    if kwargs:
        # send something to standard input
        if 'stdin' in kwargs:
            p.stdin.write(kwargs['stdin'])
    return p.communicate()

def pythonexec(user, server, python_script, wait=True, *args):
    """Execute python script on 'server'.
    If 'args' are not empty, add them as arguments to 'python_script'.
    Return (output, error) tuple as result of executing the script if 'wait' 
    is True.
    """
    _args = " ".join(args) if args else ""
    this_server = socket.gethostname()
    if this_server != server:
        command = "ssh -o ConnectTimeout=5 {0}@{1} python < {2} - {3}".format(
                                        user, server, python_script, _args)
    else:
        command = "python {0} {1}".format(python_script, _args)
    if wait:
        prog = subprocess.Popen(command, stdout=subprocess.PIPE, 
                                stderr=subprocess.PIPE, shell=True)
        return prog.communicate()
    subprocess.Popen(command, stdout=subprocess.PIPE, 
                        stderr=subprocess.PIPE, shell=True)
